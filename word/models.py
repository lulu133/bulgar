from django.db import models


class Words(models.Model):
	russian_word = models.CharField(default='', blank=True, null=True, max_length=200)
	bulgarian_word = models.CharField(default='', blank=True, null=True, max_length=200)
	theme = models.ForeignKey('Theme', default='', blank=True, null=True, on_delete=models.SET_NULL)
	subtheme = models.ForeignKey('SubTheme', default='', blank=True, null=True, on_delete=models.SET_NULL)
	wrong_one = models.CharField(default='', blank=True, null=True, max_length=200)
	wrong_two = models.CharField(default='', blank=True, null=True, max_length=200)
	image = models.ImageField(upload_to='wordsimg/', default='', blank=True, null=True)

	class Meta:
		ordering = ['?']

	def __unicode__(self):
		return self.russian_word

	def __str__(self):
		return self.russian_word

class Theme(models.Model):
	name = models.CharField(default='', blank=True, null=True, max_length=200)
	show_name = models.CharField(default='', blank=True, null=True, max_length=200)
	image = models.ImageField(upload_to='themeimg/', default='', blank=True, null=True)

	def __unicode__(self):
		return self.show_name

	def __str__(self):
		return self.show_name

class SubTheme(models.Model):
	name = models.CharField(default='', blank=True, null=True, max_length=200)
	show_name = models.CharField(default='', blank=True, null=True, max_length=200)
	theme = models.ForeignKey('Theme', default='', blank=True, null=True, on_delete=models.SET_NULL)
	image = models.ImageField(upload_to='themeimg/', default='', blank=True, null=True)

	def __unicode__(self):
		return self.show_name

	def __str__(self):
		return self.show_name
