# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-01 15:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('word', '0014_auto_20171101_1718'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='words',
            name='subtheme',
        ),
        migrations.AlterField(
            model_name='subtheme',
            name='theme',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='word.Theme'),
            preserve_default=False,
        ),
    ]
