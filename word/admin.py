from django.contrib import admin
from .models import *

class WordAdmin(admin.ModelAdmin):
	list_display = ['russian_word', 'bulgarian_word', 'subtheme', 'image']
	list_filter = ['theme', 'subtheme']
	search_fields = ['russian_word', 'bulgarian_word']
	list_per_page = 50

class ThemeAdmin(admin.ModelAdmin):
	list_display = ['name', 'show_name', 'image']



admin.site.register(Words, WordAdmin)
admin.site.register(Theme, ThemeAdmin)
admin.site.register(SubTheme)
