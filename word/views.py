from django.shortcuts import render, redirect
from .models import *
from .forms import *

def theme_list(request):
    themes = Theme.objects.all()
    return render(request, 'word/themes.html', {'themes': themes})

def word_list(request, name):
    showtheme = Theme.objects.get(name=name)
    showword = Words.objects.filter(theme=showtheme)[:1]
    return render(request, 'word/word_list.html', {'showword': showword})

def word_new(request):
	if request.method == "POST":
		form = WordForm(request.POST)
		if form.is_valid():
			words = form.save(commit=False)
			words.save()
			return redirect('word_new')
	else:
		form =WordForm()
	return render(request, 'word/add_word.html', {'form': form})