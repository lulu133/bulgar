from django import forms
from .models import *

class WordForm(forms.ModelForm):
	class Meta:
		model = Words
		fields = ('russian_word', 'bulgarian_word', 'theme', 'wrong_one', 'wrong_two', 'image')