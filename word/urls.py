from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from . import views

urlpatterns = [
	url(r'^$', views.theme_list, name='theme_list'),
	url(r'^new/$', views.word_new, name='word_new'),
	url(r'^(?P<name>[\w.@+-]+)/$', views.word_list, name='word_list'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)